﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using MyAwesomeMorpionLibrary.DAL;
using MyAwesomeMorpionLibrary.Models;

namespace MyAwesomeMorpionApi.Socket
{
    public class ChatHub : Hub
    {
        PlayerDAL playerDAL;
        GameDAL gameDAL;

        public ChatHub(ICommonDAL<Player> playerDAL, ICommonDAL<Game> gameDAL)
        {
            this.playerDAL = (PlayerDAL)playerDAL;
            this.gameDAL = (GameDAL)gameDAL;
        }

        public async Task Register(int claimerId, string message)
        {
            Player claimer = await playerDAL.GetById(claimerId);
            claimer.Connexion = Context.ConnectionId;
            playerDAL.Update(claimer);
            await Clients.All.SendAsync("Registered", claimer.Name, "Vous venez d'être connecté au serveur");
        }

        public async Task NewParty(string user, string opponentName)
        {
            Player claimer = playerDAL.GetByName(user);
            Player opponent = playerDAL.GetByName(opponentName);
            
            try
            {
                // create the new game
                var party = new Game();
                party.ClaimantPlayerId = claimer.PlayerId;
                party.CurrentPlayerId = claimer.PlayerId;
                party.Size = 3;
                gameDAL.Add(party, opponent.PlayerId);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            await Clients.All.SendAsync("NewParty", opponent.Name, claimer.Name);
        }

        public async Task AcceptParty(string opponentName, string partyId)
        {
            await Clients.All.SendAsync("PartyAccepted", opponentName, partyId);
        }

        public async Task NextRound(string opponentName, string partyId)
        {
            await Clients.All.SendAsync("NextRound", opponentName, partyId);
        }
    }
}
