﻿using Microsoft.AspNetCore.SignalR;
using MyAwesomeMorpionLibrary.DAL;
using MyAwesomeMorpionLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAwesomeMorpionApi.Socket
{
    public class CustomUserIdProvider : IUserIdProvider
    {
        PlayerDAL playerDAL;
        public CustomUserIdProvider(ICommonDAL<Player> playerDAL)
        {
            this.playerDAL = (PlayerDAL)playerDAL;
        }
        public virtual string GetUserId(HubConnectionContext connection)
        {
            return playerDAL.GetStrIdByConnectionId(connection.ConnectionId);
        }
    }
}
