﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyAwesomeMorpionLibrary.DAL;
using MyAwesomeMorpionLibrary.Models;

namespace MyAwesomeMorpionApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        private GameDAL gameDAL;

        public GameController(ICommonDAL<Game> gameDAL)
        {
            this.gameDAL = (GameDAL)gameDAL;
        }
        // GET api/values
        [HttpGet]
        async public Task<ActionResult<IEnumerable<Game>>> Get()
        {
            var all = await gameDAL.GetAll();
            return all;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        async public Task<ActionResult<Game>> Get(int id)
        {
            return await gameDAL.GetById(id);
        }

        // POST api/values
        [HttpPost]
        public Game Post([FromBody] Game value)
        {
            return gameDAL.Add(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public Game Put(int id, [FromBody] Game value)
        {
            // remove player game to avoid already track entity problem
            value.PlayerGames = null;
            return gameDAL.Update(value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            gameDAL.Delete(id);
        }

        // GET api/game/newgame/5/4
        [HttpGet("new/{connectedPlayerId}/{selectedPlayerId}")]
        public void RegisterNewGame(int connectedPlayerId, int selectedPlayerId, int size = 3)
        {
            // create the new game
            var party = new Game();
            party.ClaimantPlayerId = connectedPlayerId;
            party.CurrentPlayerId = connectedPlayerId;
            party.Size = size;
            gameDAL.Add(party, selectedPlayerId);
        }
    }
}
