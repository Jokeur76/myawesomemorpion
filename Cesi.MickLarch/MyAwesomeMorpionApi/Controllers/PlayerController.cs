﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyAwesomeMorpionApi.Socket;
using MyAwesomeMorpionLibrary.DAL;
using MyAwesomeMorpionLibrary.Models;

namespace MyAwesomeMorpionApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayerController : ControllerBase
    {
        private PlayerDAL playerDAL;
        private GameDAL gameDAL;

        public PlayerController(ICommonDAL<Player> playerDAL, ICommonDAL<Game> gameDAL)
        {
            this.playerDAL = (PlayerDAL)playerDAL;
            this.gameDAL = (GameDAL)gameDAL;
        }

        // GET api/values
        [HttpGet]
        async public Task<ActionResult<IEnumerable<Player>>> Get()
        {
            return await playerDAL.GetAll();
        }

        // GET api/values
        [HttpGet("connected")]
        public ActionResult<IEnumerable<Player>> GetConnected()
        {
            return playerDAL.Find(p => p.Connected).ToList();
        }

        // GET api/values/5
        [HttpGet("{id:int}")]
        async public Task<ActionResult<Player>> Get(int id)
        {
            return await playerDAL.GetById(id);
        }

        // GET api/values/name/toto
        [HttpGet("name/{name}")]
        public ActionResult<Player> GetByName(string name)
        {
            return playerDAL.GetByName(name);
        }

        // POST api/values
        [HttpPost]
        public Player Post([FromBody] Player value)
        {
            Player existPlayer = playerDAL.GetByName(value.Name);
            if (existPlayer != null)
                throw new Exception("Name already in use");
            return playerDAL.Add(value);
        }

        // PUT api/values/5
        [HttpPut("{id:int}")]
        public Player Put(int id, [FromBody] Player value)
        {
            return playerDAL.Update(value);
        }

        // DELETE api/values/5
        [HttpDelete("{id:int}")]
        public void Delete(int id)
        {
            playerDAL.Delete(id);
        }

        // Put api/values/5/connect
        [HttpPut("{id:int}/connect")]
        async public Task<Player> ConnectPlayer(int id, [FromBody] Player value)
        {
            var dbPlayer = await playerDAL.GetById(id);
            if (dbPlayer != null && dbPlayer.Password == value.Password)
            {
                dbPlayer.Connected = true;
                playerDAL.Update(dbPlayer);
                return dbPlayer;
            }
            throw new Exception("Bad password or login");

        }

        // GET api/values/5/games
        [HttpGet("{id:int}/games")]
        public List<Game> GetPlayerGames(int id)
        {
            return playerDAL.GetGames(id);
        }

        // GET api/values/5/accepted
        [HttpGet("{id:int}/accepted")]
        async public Task<List<Game>> GetAcceptedGames(int id)
        {
            return await playerDAL.GetAcceptedGames(id);
        }

        // Put api/values/5/disconnect
        [HttpPut("{id}/disconnect")]
        public void DisconnectPlayer(int id, [FromBody] Player value)
        {
            value.Connected = false;
            value.CurrentGame = null;
            foreach (Game game in value.LaunchGames)
            {
                gameDAL.Delete(game.GameId);
                value.LaunchGames.Remove(game);
            }
            playerDAL.Update(value);
        }
    }
}
