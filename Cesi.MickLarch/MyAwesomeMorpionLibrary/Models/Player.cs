﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MyAwesomeMorpionLibrary.Models
{
    public class Player
    {
        public int PlayerId { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string StyleType { get; set; }
        public string Connexion { get; set; }
        public int WinGames { get; set; } = 0;
        public int LooseGames { get; set; } = 0;
        public int DrawGames { get; set; } = 0;
        public bool Auto { get; set; }
        public bool Connected { get; set; }
        public ICollection<Game> LaunchGames { get; } = new List<Game>();
        public Game CurrentGame { get; set; } = new Game();
        public int? PlayingGame { get; set; }
        public ICollection<PlayerGame> PlayerGames { get; set; } = new List<PlayerGame>();

        public Player() { }

        public Player(string _name, string _type, bool _auto, int _drawGames, int _winGames, int _looseGames)
        {
            Name = _name;
            StyleType = _type;
            Auto = _auto;
            WinGames = _winGames;
            LooseGames = _looseGames;
            DrawGames = _drawGames;
        }

        public Player(string _name, string _type, bool _connected, bool _auto = false)
        {
            Name = _name;
            StyleType = _type;
            Auto = _auto;
            Connected = _connected;
        }

        const string ROUND_TYPE = "O";
        const string CROSS_TYPE = "X";

    }
}
