﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAwesomeMorpionLibrary.Models
{
    public class Game
    {
        public int GameId { get; set; }
        public List<Move> MovesList { get; set; }
        public int Size { get; set; }

        public int ClaimantPlayerId { get; set; }
        public Player ClaimantPlayer { get; set; }
        
        public int CurrentPlayerId { get; set; }
        public Player CurrentPlayer { get; set; }

        public int? Winner { get; set; }
        public bool Started { get; set; }
        public bool Finished { get; set; }

        public ICollection<PlayerGame> PlayerGames { get; set; } = new List<PlayerGame>();
        //public ICollection<Player> PlayingPlayers { get; } = new List<Player>();
    }
}
