﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyAwesomeMorpionLibrary.Models
{
    public class PlayerGame
    {
        public int PlayerId { get; set; }
        public int GameId { get; set; }
    }
}
