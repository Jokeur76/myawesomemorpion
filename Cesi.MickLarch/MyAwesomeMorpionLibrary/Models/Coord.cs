﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAwesomeMorpionLibrary.Models
{
    [NotMapped]
    public class Coord
    {
        private string x;
        private string y;
        private string name;

        public string X
        {
            get { return x; }
            set { x = value; }
        }

        public string Y
        {
            get { return y; }
            set { y = value; }
        }
        
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }
}
