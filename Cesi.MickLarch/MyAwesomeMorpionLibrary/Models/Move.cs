﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MyAwesomeMorpionLibrary.Models
{
    public class Move
    {
        private int id;
        private string name;
        private string styleType;
        public string x { get; set; } 
        public string y { get; set; } 
        private int gameId;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set 
            {
                string[] strCoord = value.Replace("Field", "").Split('I');
                x = strCoord[0];
                y = strCoord[1];
                name = value; 
            }
        }

        public string StyleType
        {
            get { return styleType; }
            set { styleType = value; }
        }

        public Coord Coordinates
        {
            get { return new Coord() { X = x, Y = y }; }
            set { 
                x = value.X; 
                y = value.Y; 
            }
        }

        public int GameId
        {
            get { return gameId; }
            set { gameId = value; }
        }

        public Game Game { get; set; }
    }
}
