﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyAwesomeMorpionLibrary.Migrations
{
    public partial class AddColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PlayerOId",
                table: "Games");

            migrationBuilder.RenameColumn(
                name: "PlayerXId",
                table: "Games",
                newName: "ClaimantPlayerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ClaimantPlayerId",
                table: "Games",
                newName: "PlayerXId");

            migrationBuilder.AddColumn<int>(
                name: "PlayerOId",
                table: "Games",
                nullable: false,
                defaultValue: 0);
        }
    }
}
