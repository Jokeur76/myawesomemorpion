﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyAwesomeMorpionLibrary.Migrations
{
    public partial class AddSizeAndCurrentPlayerToGame : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CurrentPlayerId",
                table: "Games",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Size",
                table: "Games",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Games_CurrentPlayerId",
                table: "Games",
                column: "CurrentPlayerId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Games_Players_CurrentPlayerId",
                table: "Games",
                column: "CurrentPlayerId",
                principalTable: "Players",
                principalColumn: "PlayerId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Games_Players_CurrentPlayerId",
                table: "Games");

            migrationBuilder.DropIndex(
                name: "IX_Games_CurrentPlayerId",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "CurrentPlayerId",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "Size",
                table: "Games");
        }
    }
}
