﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyAwesomeMorpionLibrary.Migrations
{
    public partial class AddClaimantToGame : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Games_ClaimantPlayerId",
                table: "Games",
                column: "ClaimantPlayerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Games_Players_ClaimantPlayerId",
                table: "Games",
                column: "ClaimantPlayerId",
                principalTable: "Players",
                principalColumn: "PlayerId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Games_Players_ClaimantPlayerId",
                table: "Games");

            migrationBuilder.DropIndex(
                name: "IX_Games_ClaimantPlayerId",
                table: "Games");
        }
    }
}
