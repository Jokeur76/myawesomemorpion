﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyAwesomeMorpionLibrary.Migrations
{
    public partial class ModifyCoordManagement4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "x",
                table: "Move",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "y",
                table: "Move",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "x",
                table: "Move");

            migrationBuilder.DropColumn(
                name: "y",
                table: "Move");
        }
    }
}
