﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MyAwesomeMorpionLibrary;

namespace MyAwesomeMorpionLibrary.Migrations
{
    [DbContext(typeof(MorpionContext))]
    [Migration("20190109211606_AddMoveToDB")]
    partial class AddMoveToDB
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.0-rtm-35687")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MyAwesomeMorpionLibrary.Models.Game", b =>
                {
                    b.Property<int>("GameId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ClaimantPlayerId");

                    b.Property<int>("CurrentPlayerId");

                    b.Property<int>("Size");

                    b.HasKey("GameId");

                    b.HasIndex("ClaimantPlayerId");

                    b.HasIndex("CurrentPlayerId")
                        .IsUnique();

                    b.ToTable("Games");
                });

            modelBuilder.Entity("MyAwesomeMorpionLibrary.Models.Move", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("GameId");

                    b.Property<string>("Name");

                    b.Property<string>("StyleType");

                    b.HasKey("Id");

                    b.HasIndex("GameId");

                    b.ToTable("Move");
                });

            modelBuilder.Entity("MyAwesomeMorpionLibrary.Models.Player", b =>
                {
                    b.Property<int>("PlayerId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("Auto");

                    b.Property<bool>("Connected");

                    b.Property<int>("DrawGames");

                    b.Property<int>("LooseGames");

                    b.Property<string>("Name");

                    b.Property<string>("Password");

                    b.Property<string>("StyleType");

                    b.Property<int>("WinGames");

                    b.HasKey("PlayerId");

                    b.ToTable("Players");
                });

            modelBuilder.Entity("MyAwesomeMorpionLibrary.Models.PlayerGame", b =>
                {
                    b.Property<int>("PlayerId");

                    b.Property<int>("GameId");

                    b.HasKey("PlayerId", "GameId");

                    b.HasIndex("GameId");

                    b.ToTable("PlayerGame");
                });

            modelBuilder.Entity("MyAwesomeMorpionLibrary.Models.Game", b =>
                {
                    b.HasOne("MyAwesomeMorpionLibrary.Models.Player", "ClaimantPlayer")
                        .WithMany("LaunchGames")
                        .HasForeignKey("ClaimantPlayerId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("MyAwesomeMorpionLibrary.Models.Player", "CurrentPlayer")
                        .WithOne("CurrentGame")
                        .HasForeignKey("MyAwesomeMorpionLibrary.Models.Game", "CurrentPlayerId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("MyAwesomeMorpionLibrary.Models.Move", b =>
                {
                    b.HasOne("MyAwesomeMorpionLibrary.Models.Game", "Game")
                        .WithMany("MovesList")
                        .HasForeignKey("GameId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("MyAwesomeMorpionLibrary.Models.PlayerGame", b =>
                {
                    b.HasOne("MyAwesomeMorpionLibrary.Models.Game")
                        .WithMany("PlayerGames")
                        .HasForeignKey("GameId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("MyAwesomeMorpionLibrary.Models.Player")
                        .WithMany("PlayerGames")
                        .HasForeignKey("PlayerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
