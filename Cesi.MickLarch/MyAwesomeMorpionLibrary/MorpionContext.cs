﻿using Microsoft.EntityFrameworkCore;
using MyAwesomeMorpionLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAwesomeMorpionLibrary
{
    public class MorpionContext: DbContext
    {
        // plus ou moins une representation des tables
        public DbSet<Game> Games { get; set; }
        public DbSet<Player> Players { get; set; }

        public MorpionContext(DbContextOptions<MorpionContext> options)
            :base(options)
        {
        }

        public MorpionContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Move>()
                .HasOne<Game>(m => m.Game)
                .WithMany(g => g.MovesList)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Game>()
                .HasOne<Player>(g => g.ClaimantPlayer)
                .WithMany(p => p.LaunchGames)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Game>()
                .HasOne<Player>(g => g.CurrentPlayer)
                .WithOne(p => p.CurrentGame)
                .OnDelete(DeleteBehavior.Restrict);
            //modelBuilder.Entity<Player>()
            //    .HasOne<Game>(p => p.PlayingGame)
            //    .WithMany(g => g.PlayingPlayers)
            //    .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<PlayerGame>()
                .HasKey(pg => new { pg.PlayerId, pg.GameId });
        }
    }
}
