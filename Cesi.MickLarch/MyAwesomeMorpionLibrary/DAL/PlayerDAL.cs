﻿using MyAwesomeMorpionLibrary.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace MyAwesomeMorpionLibrary.DAL
{
    public class PlayerDAL: CommonDAL<Player>
    {
        public PlayerDAL(MorpionContext morpionContext): base(morpionContext)
        {
        }

        async public new Task<List<Player>> GetAll()
        {
            return await morpionContext.Players
                .Include(p => p.LaunchGames)
                .Include(p => p.PlayerGames)
                .ToListAsync();
        }

        async public new Task<Player> GetById(int id)
        {
            return await morpionContext.Players
                .Include(p => p.LaunchGames)
                .Include(p => p.PlayerGames)
                .SingleAsync(g => g.PlayerId == id);
        }

        public string GetStrIdByConnectionId(string connectionId)
        {
            return morpionContext.Players
                .Single(g => g.Connexion == connectionId)
                .PlayerId.ToString();
        }

        public Player GetByName(string name)
        {
            return Find(p => p.Name == name).SingleOrDefault();
        }

        public List<Game> GetGames(int id)
        {
            return morpionContext.Games
                .Include(g => g.PlayerGames)
                .Include(g => g.ClaimantPlayer)
                .Where(g => g.PlayerGames.Any(pg => pg.PlayerId == id) && !g.Finished)
                .ToList();
        }

        async public Task<List<Game>> GetAcceptedGames(int id)
        {
            Player current = await GetById(id);
            IEnumerable<int> opponentsList = current.PlayerGames
                .Where(pg => pg.PlayerId != current.PlayerId)
                .Select(pg => pg.PlayerId).Distinct();
            var acceptedParty = morpionContext.Players
                .Where(p => opponentsList.Contains(p.PlayerId) && p.PlayingGame != null)
                .Select(p => p.PlayingGame).AsEnumerable();
            return morpionContext.Games.Where(g => acceptedParty.Contains(g.GameId)).ToList();
        }
    }
}
