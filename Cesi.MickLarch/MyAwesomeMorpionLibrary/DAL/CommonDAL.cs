﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace MyAwesomeMorpionLibrary.DAL
{
    public class CommonDAL<T> : ICommonDAL<T> where T:class
    {
        protected MorpionContext morpionContext;
        protected DbSet<T> set;

        public CommonDAL(MorpionContext morpionContext)
        {
            this.morpionContext = morpionContext;
        }

        public T Add(T entity)
        {
            morpionContext.Set<T>().Add(entity);
            morpionContext.SaveChanges();
            return entity;
        }

        public void Delete(int id)
        {
            T toDel = morpionContext.Set<T>().Find(id);
            if(toDel != null)
            {
                morpionContext.Remove(toDel);
                morpionContext.SaveChanges();
            }
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> findExpression)
        {
            return morpionContext.Set<T>().Where(findExpression);
        }

        public List<T> GetAll()
        {
            return morpionContext.Set<T>().ToList();
        }

        public T GetById(int id)
        {
            return morpionContext.Set<T>().Find(id);
        }

        public T Update(T entity)
        {
            if (morpionContext.Entry(entity).State.Equals(EntityState.Detached))
            {
                morpionContext.Set<T>().Update(entity);
            }
            morpionContext.SaveChanges();
            return entity;
        }

        public void Dispose()
        {
            morpionContext.Dispose();
        }
    }
}
