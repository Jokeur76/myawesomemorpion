﻿using MyAwesomeMorpionLibrary.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace MyAwesomeMorpionLibrary.DAL
{
    public class GameDAL: CommonDAL<Game>
    {
        public GameDAL(MorpionContext morpionContext): base(morpionContext)
        {
        }

        public Game Add(Game value, int selectedPlayerId)
        {
            morpionContext.Games.Add(value);
            morpionContext.SaveChanges();
            morpionContext.AddRange(
                new PlayerGame { PlayerId = value.ClaimantPlayerId, GameId = value.GameId },
                new PlayerGame { PlayerId = selectedPlayerId, GameId = value.GameId }
            );
            morpionContext.SaveChanges();
            return value;
        }

        async public new Task<List<Game>> GetAll()
        {
            return await morpionContext.Games
                .Include(g => g.PlayerGames)
                .Include(g => g.ClaimantPlayer)
                .Include(g => g.CurrentPlayer)
                .Include(g => g.MovesList)
                .ToListAsync();
        }

        async public new Task<Game> GetById(int id)
        {
            return await morpionContext.Games
                .Include(g => g.PlayerGames)
                .Include(g => g.ClaimantPlayer)
                .Include(g => g.CurrentPlayer)
                .Include(g => g.MovesList)
                .SingleAsync(g => g.GameId == id);
        }
    }
}
