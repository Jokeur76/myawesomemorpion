﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MyAwesomeMorpionLibrary.DAL
{
    public interface ICommonDAL<T>: IDisposable
    {
        List<T> GetAll();
        T GetById(int Id);
        T Add (T entity);
        T Update(T entity);
        void Delete(int id);
        IEnumerable<T> Find(Expression<Func<T, bool>> findExpression);
    }
}
