﻿using Cesi.MickLarch.MyAwesomeMorpion.Services;
using Cesi.MickLarch.MyAwesomeMorpion.ViewModels;
using Cesi.MickLarch.MyAwesomeMorpion.Views;
using Microsoft.AspNetCore.SignalR.Client;
using MyAwesomeMorpionLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Unity.Attributes;

namespace Cesi.MickLarch.MyAwesomeMorpion
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private PlayersService playersService = new PlayersService();
        private List<Player> allPlayers;

        // socket
        HubConnection connection;

        public MainWindow()
        {
            InitializeComponent();
         
            // socket
            connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:44393/chatHub")
                .Build();
            
            connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0,5) * 1000);
                await connection.StartAsync();
            };

            LoadPlayers();
            this.DataContext = new Main(allPlayers, connection);

            this.mainFrame.NavigationService.Navigate(new VHome(mainFrame.NavigationService, connection));
        }

        async private void LoadPlayers()
        {
            allPlayers = await playersService.GetPlayersList();
            ListJoueurX.Items.Clear();
            ListJoueurO.Items.Clear();
            foreach (Player player in allPlayers)
            {
                ListJoueurX.Items.Add(player.Name);
                ListJoueurO.Items.Add(player.Name);
            }
        }

        public void NewGame_Button_Click(object sender, RoutedEventArgs e)
        {
            (DataContext as Main).LaunchGame(
                ListJoueurX, JoueurX,
                ListJoueurO, JoueurO,
                mainFrame,
                GameSize
            );
        }
        public void ResetGame_Button_Click(object sender, RoutedEventArgs e) {
            string currentJoueurO = JoueurO.Text == "" ? (string)ListJoueurO.SelectedValue : JoueurO.Text;
            string currentJoueurX = JoueurX.Text == "" ? (string)ListJoueurX.SelectedValue : JoueurX.Text;
            JoueurO.Text = "";
            JoueurX.Text = "";
            LoadPlayers();
            ListJoueurO.SelectedIndex = ListJoueurO.Items.IndexOf(currentJoueurO);
            ListJoueurX.SelectedIndex = ListJoueurX.Items.IndexOf(currentJoueurX);
            (DataContext as Main).ResetGame(
                ListJoueurX, JoueurX,
                ListJoueurO, JoueurO,
                mainFrame,
                GameSize
            );
        }

        public void GoScore_Button_Click(object sender, RoutedEventArgs e) {
            mainFrame.Navigate(new VScores());
        }
    }
}
