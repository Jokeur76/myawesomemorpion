﻿using Cesi.MickLarch.MyAwesomeMorpion.ViewModels;
using MyAwesomeMorpionLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cesi.MickLarch.MyAwesomeMorpion.Views
{
    /// <summary>
    /// Logique d'interaction pour Scores.xaml
    /// </summary>
    public partial class VScores : Page
    {
        public VScores()
        {
            ScoresVM scoresVM = new ScoresVM();
            InitializeComponent();
            this.DataContext = scoresVM;
            PlayersDataGrid.AutoGeneratingColumn += PlayersDataGrid_AutoGeneratingColumn;
        }

        void PlayersDataGrid_AutoGeneratingColumn(object sender, System.Windows.Controls.DataGridAutoGeneratingColumnEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "PlayStyle":
                    e.Cancel = true;
                    break;
                case "StyleType":
                    e.Cancel = true;
                    break;
                case "Name":
                    e.Column.Header = "Nom du joueur";
                    break;
                case "WinGames":
                    e.Column.Header = "Parties gagnées";
                    break;
                case "LooseGames":
                    e.Column.Header = "Parties perdues";
                    break;
                case "DrawGames":
                    e.Column.Header = "Match nuls";
                    break;

            }
        }

        private void GoHome(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }

        private void DataGridRow_MouseDoubleClick(object row, MouseButtonEventArgs e)
        {
            (DataContext as ScoresVM).DisplayPieChart((Player)(row as DataGridRow).DataContext);
        }

        private void PieChart_DataClick(object sender, LiveCharts.ChartPoint chartPoint)
        {

        }
    }
}
