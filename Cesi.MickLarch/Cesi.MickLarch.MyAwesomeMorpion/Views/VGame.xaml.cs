﻿using Cesi.MickLarch.MyAwesomeMorpion.Services;
using Cesi.MickLarch.MyAwesomeMorpion.ViewModels;
using Microsoft.AspNetCore.SignalR.Client;
using MyAwesomeMorpionLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Unity.Attributes;

namespace Cesi.MickLarch.MyAwesomeMorpion.Views
{
    /// <summary>
    /// Logique d'interaction pour Game.xaml
    /// </summary>
    public partial class VGame : Page
    {

        public VGame(List<Player> playersList, Game game, HubConnection connection)
        {
            InitializeComponent();
            CreateGrid(game.Size);
            this.DataContext = new GameVM(playersList, GameGrid, game, connection);
        }

        public void ForceUpdate(object sender, RoutedEventArgs e) 
        { 
            (DataContext as GameVM).UpdateParty();
        }
        async public void FieldClick(object sender, RoutedEventArgs e) {
            Player nextPlayer = await (DataContext as GameVM).FieldClick(sender, e);
            if ("X".Equals(nextPlayer.StyleType))
            {
                PlayerXScore.Style = (Style)Application.Current.Resources["SelectedScore"];
                PlayerOScore.Style = null;
            } 
            else
            {
                PlayerXScore.Style = null;
                PlayerOScore.Style = (Style)Application.Current.Resources["SelectedScore"];
            }
        }

        private void CreateGrid(int gameSize)
        {
            ColumnDefinition col;
            RowDefinition row;
            Button btn;

            for(int x = 0; x < gameSize; x++) {
                col = new ColumnDefinition();
                col.Width = new GridLength(1, GridUnitType.Star);
                GameGrid.ColumnDefinitions.Add(col);
                row = new RowDefinition();
                row.Height = new GridLength(1, GridUnitType.Star);
                GameGrid.RowDefinitions.Add(row);
                for(int y = 0; y < gameSize; y++) {
                    btn = new Button();
                    btn.Name = (string)$"Field{x}I{y}";
                    btn.SetValue(Grid.ColumnProperty, x);
                    btn.SetValue(Grid.RowProperty, y);
                    btn.Click += FieldClick;
                    GameGrid.Children.Add(btn);
                }
            }
        }
    }
}
