﻿using MyAwesomeMorpionLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Cesi.MickLarch.MyAwesomeMorpion.Services;
using System.Windows.Threading;
using Microsoft.AspNetCore.SignalR.Client;

namespace Cesi.MickLarch.MyAwesomeMorpion.Views
{
    /// <summary>
    /// Logique d'interaction pour VHome.xaml
    /// </summary>
    public partial class VHome : Page
    {
        private Player connectedPlayer;
        private List<Player> playersList;
        private List<Player> connectedPlayersList;
        private List<Game> playersProposalList;

        // services
        private PlayersService ps = new PlayersService();
        private GamesService gs = new GamesService();
        
        // timers
        DispatcherTimer connectedPlayersTimer;
        DispatcherTimer playerGamesTimer;
        DispatcherTimer searchAcceptedTimer;

        // socket
        HubConnection connection;
        NavigationService nav;
        
        public VHome(NavigationService _nav, HubConnection connection)
        {
            this.nav = _nav;
            InitializeComponent();
            SetPlayersAsync();
            // connectedPlayersTimer
            connectedPlayersTimer = new DispatcherTimer();
            connectedPlayersTimer.Tick += new EventHandler(connectedPlayersTimer_Tick);
            connectedPlayersTimer.Interval = new TimeSpan(0,0,1);
            //connectedPlayersTimer.Start();
            // playerGamesTimer
            playerGamesTimer = new DispatcherTimer();
            playerGamesTimer.Tick += new EventHandler(playerGamesTimer_Tick);
            playerGamesTimer.Interval = new TimeSpan(0,0,1);
            //playerGamesTimer.Start();
            // searchAcceptedTimer
            searchAcceptedTimer = new DispatcherTimer();
            searchAcceptedTimer.Tick += new EventHandler(searchAcceptedTimer_Tick);
            searchAcceptedTimer.Interval = new TimeSpan(0,0,1);
            //searchAcceptedTimer.Start();

            // socket
            this.connection = connection;

            PlayersList.MouseDoubleClick += Ask_For_Game_Click;
            GamesList.MouseDoubleClick += Accept_Game_Click;
        }

        private void connectButton_Click(object sender, RoutedEventArgs e)
        {
            ConnectServ();
        }

        private async void ConnectServ()
        {
            connection.On<string, string>("Registered", (user, message) =>
            {
                if(connectedPlayer != null)
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        if (connectedPlayer.Name == user)
                            MessageBox.Show(message);
                        SetConnectedPlayer();
                        SetPlayerGames();
                    });
                }
            });
            
            connection.On<string, string>("NewParty", (user, opponent) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    if(connectedPlayer != null)
                    {
                        if (connectedPlayer.Name == user)
                        {
                            //MessageBox.Show(String.Format("{0}, {1} vous propose une partie", user, opponent));
                            SetPlayerGames();
                        }
                    }
                });
            });

            connection.On<string, string>("PartyAccepted", (opponentName, partyId) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    AcceptParty(partyId, opponentName);
                });
            });

            try
            {
                await connection.StartAsync();
                await connection.InvokeAsync("Register", 
                    connectedPlayer.PlayerId, "");
            }
            catch (Exception ex)
            {                
                MessageBox.Show("Register failed");
                Console.WriteLine(ex.Message);
            }
        }
        
        async private void AcceptParty(string partyId, string opponentName)
        {
            Game selectedGame = await gs.GetById(Convert.ToInt32(partyId));
            //MessageBox.Show(String.Format("{0} a accepté votre partie", opponentName));
            LaunchMutliGame(selectedGame, opponentName);
        }
        private void connectedPlayersTimer_Tick(object sender, EventArgs e)
        {
            SetConnectedPlayer();
        }
        
        private void playerGamesTimer_Tick(object sender, EventArgs e)
        {
            SetPlayerGames();
        }

        async private void searchAcceptedTimer_Tick(object sender, EventArgs e)
        {
            if (connectedPlayer != null)
            {
                List<Game> acceptedGame = await ps.GetAcceptedFor(connectedPlayer.PlayerId);
                if (acceptedGame.Any())
                    LaunchMutliGame(acceptedGame.First());
            }
        }

        public async void SetPlayersAsync()
        {
            playersList = await ps.GetPlayersList();
            PlayersListSelect.ItemsSource = playersList.Select(p => p.Name);
        }

        private void Switch_To_Inscription_Click(object sender, RoutedEventArgs e)
        {
            PlayersListSelect.Visibility = Visibility.Collapsed;
            NoAccount.Visibility = Visibility.Collapsed;
            Connexion.Visibility = Visibility.Collapsed;
            
            PlayerName.Visibility = Visibility.Visible;
            AlreadyRegistered.Visibility = Visibility.Visible;
            Inscription.Visibility = Visibility.Visible;
        }

        private void Switch_To_Connexion_Click(object sender, RoutedEventArgs e)
        {
            SetPlayersAsync();

            PlayersListSelect.Visibility = Visibility.Visible;
            NoAccount.Visibility = Visibility.Visible;
            Connexion.Visibility = Visibility.Visible;
            
            PlayerName.Visibility = Visibility.Collapsed;
            AlreadyRegistered.Visibility = Visibility.Collapsed;
            Inscription.Visibility = Visibility.Collapsed;
        }

        private async void Connexion_Click(object sender, RoutedEventArgs e)
        {
            var choosedName = (string)PlayersListSelect.SelectedValue;
            Player choosedPlayer = playersList.Where(p => p.Name == choosedName).Single();
            choosedPlayer.Password = Password.Text;
            choosedPlayer.Connected = true;
            connectedPlayer = await ps.ConnectPlayer(choosedPlayer);
            
            if (connectedPlayer != null)
            {
                ConnectServ();
                ConnexionContainer.Visibility = Visibility.Hidden;
                deconnexionContainer.Visibility = Visibility.Visible;
            }
        }

        private async void SetConnectedPlayer()
        {
            connectedPlayersList = await ps.GetConnectedPlayersList();
            if (connectedPlayer != null && connectedPlayersList != null && connectedPlayersList.Count > 0)
                PlayersList.ItemsSource = connectedPlayersList.Where(p => p.PlayerId != connectedPlayer.PlayerId).Select(p => p.Name);
        }

        private async void SetPlayerGames()
        {
            if (connectedPlayer != null)
                playersProposalList = await ps.GetProposalsListForPlayer(connectedPlayer);
                if (playersProposalList != null && playersProposalList.Count > 0 && playersProposalList.All(p => p.ClaimantPlayer != null))
                    GamesList.ItemsSource = playersProposalList.Select(p => p.ClaimantPlayer.Name);
        }

        private async void Inscription_Click(object sender, RoutedEventArgs e)
        {
            Player newPlayer = new Player(PlayerName.Text, null, true);
            newPlayer.Password = Password.Text;
            connectedPlayer = await ps.RegisterPlayer(newPlayer);
            if (connectedPlayer != null)
            {
                ConnexionContainer.Visibility = Visibility.Hidden;
                deconnexionContainer.Visibility = Visibility.Visible;
            }
        }

        async private void deconnexion_Click(object sender, RoutedEventArgs e)
        {
            bool succeed = await ps.Disconnect(connectedPlayer);
            if (succeed)
            {
                ConnexionContainer.Visibility = Visibility.Visible;
                deconnexionContainer.Visibility = Visibility.Hidden;
            }
        }

        async private void Ask_For_Game_Click(object listBox, RoutedEventArgs e)
        {
            try
            {
                ListBox lb = (ListBox)listBox;
                Player selectedPlayer = playersList.Single(p => p.Name == (string)lb.SelectedValue);
                bool succeed = await gs.AskForGame(connectedPlayer, selectedPlayer);
                await connection.InvokeAsync("NewParty", 
                    connectedPlayer.Name, selectedPlayer.Name);
            }
            catch (Exception ex)
            {                
                MessageBox.Show("New party failed");
                Console.WriteLine(ex.Message);
            }
        }

        async private void Accept_Game_Click(object listBox, RoutedEventArgs e)
        {
            ListBox lb = (ListBox)listBox;
            Game selectedGame = playersProposalList.Single(p => p.ClaimantPlayer.Name == (string)lb.SelectedValue);
            try
            {
                await connection.InvokeAsync("AcceptParty", 
                    connectedPlayer.Name, selectedGame.GameId);
                LaunchMutliGame(selectedGame);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur lors de l'acceptation de la partie : " + ex.Message);
            }
        }

        async private void LaunchMutliGame(Game selectedGame, string opponentName = null)
        {
            // get players
            connectedPlayer.PlayingGame = selectedGame.GameId;
            Player opponent = connectedPlayer;
            if (opponentName != null)
            {
                opponent = await ps.GetByName(opponentName);
            }
            var playerO = FormatPlayerO(selectedGame.ClaimantPlayer, selectedGame.GameId);
            var playerX = FormatPlayerX(opponent, selectedGame.GameId);
            var players = await ps.UpdateList(new List<Player> { playerO, playerX });
            nav.Navigate(new VGame(players, selectedGame, connection));
        }

        private Player FormatPlayerO(Player player, int gameId)
        {
            player.CurrentGame = null;
            player.StyleType = "O";
            player.PlayingGame = gameId;
            return player;
        }

        private Player FormatPlayerX(Player player, int gameId)
        {
            player.CurrentGame = null;
            player.StyleType = "X";
            player.PlayingGame = gameId;
            return player;
        }
    }
}
