﻿using Cesi.MickLarch.MyAwesomeMorpion.Services;
using Microsoft.AspNetCore.SignalR.Client;
using MyAwesomeMorpionLibrary.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Cesi.MickLarch.MyAwesomeMorpion.ViewModels
{
    public enum GameLineType
    {
        HORIZONTALE,
        VERTICALE,
        DIAG_HAUT,
        DIAG_BAS
    }

    public class GameLine
    {
        private List<Move> movesList;
        private GameLineType type;

        public GameLineType Type
        {
            get { return type; }
            set { type = value; }
        }

        public List<Move> MovesList
        {
            get { return movesList; }
            set { movesList = value; }
        }

    }


    public class GameVM: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private string playerOScore;
        public string PlayerOScore
        {
            get { return playerOScore; }
            set {
                playerOScore = value;
                OnPropertyChanged();
            }
        }
        private string playerXScore;
        public string PlayerXScore
        {
            get { return playerXScore; }
            set {
                playerXScore = value;
                OnPropertyChanged();
            }
        }

        public List<GameLine> gameMatrix;

        private HashSet<Coord> gamePlate = new HashSet<Coord>();
        public HashSet<Coord> GamePlate
        {
            get { return gamePlate; }
            set { gamePlate = value; }
        }

        const string ROUND_TYPE = "O";
        const string CROSS_TYPE = "X";

        // private Player currentPlayer;
        private Player playerX;
        private Player playerO;

        private Player winner;
        // private bool gameStarted;
        private List<Player> allPlayers;
        private PlayersService playersService = new PlayersService();
        private Grid gameGrid;
        
        public Game game;
        
        // socket
        HubConnection connection;

        // services
        private PlayersService ps = new PlayersService();
        private GamesService gs = new GamesService();

        public GameVM(List<Player> playersList, Grid _gameGrid, Game game, HubConnection connection)
        {
            this.connection = connection;
            
            connection.On<string, string>("NextRound", (user, message) =>
            {
                UpdateParty();
                MessageBox.Show(String.Format("A {0} de jouer", game.CurrentPlayer.Name));
            });

            gameGrid = _gameGrid;
            this.game = game;
            CreateGameMatrix();
            GetPlayers();
            playerX = playersList.Single(p => p.StyleType.Equals(CROSS_TYPE));
            playerO = playersList.Single(p => p.StyleType.Equals(ROUND_TYPE));
            if(game.CurrentPlayer == null)
                game.CurrentPlayer = playerO;
            if (game.CurrentPlayer.Auto == true)
            {
                OrdiPlay();
            }
            game.Started = playerO.PlayingGame == game.GameId && playerX.PlayingGame == game.GameId;
            UpdateView();
        }

        private void UpdateView()
        {
            PlayerOScore = $"{playerO.Name} : {playerO.WinGames} / {playerO.LooseGames} / {playerO.DrawGames}";
            PlayerXScore = $"{playerX.Name} : {playerX.WinGames} / {playerX.LooseGames} / {playerX.DrawGames}";
            if (game.MovesList == null)
                game.MovesList = new List<Move>();
            foreach(Button btn in gameGrid.Children.OfType<Button>())
            {
                btn.Style = null;
            }
            foreach(Move move in game.MovesList)
            {
                Button btn = gameGrid.Children.OfType<Button>().Single(b => b.Name == $"Field{move.Coordinates.X}I{move.Coordinates.Y}");
                btn.Style = playersService.GetStyleFromStyleType(move.StyleType);
            }
        }

        async public void UpdateParty(bool broadcast = false)
        {
            try
            {
                game = await gs.GetById(game.GameId);
                UpdateView();
                if (broadcast)
                {
                    await connection.InvokeAsync("NextRound", 
                        game.CurrentPlayer.Name, game.GameId);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private async void GetPlayers()
        {
            allPlayers = await playersService.GetPlayersList();
        }

        private Move GetMoveFromCoord(int x, int y)
        {
            return new Move() { Name = $"Field{x}I{y}", Coordinates = new Coord() { X = $"{x}", Y = $"{y}" } };
        }

        private void CreateGameMatrix()
        {
            gameMatrix = new List<GameLine>();
            GameLine line = new GameLine() { Type = GameLineType.VERTICALE, MovesList = new List<Move>() };
            GameLine column = new GameLine() { Type = GameLineType.HORIZONTALE, MovesList = new List<Move>() };
            GameLine diagHaut = new GameLine() { Type = GameLineType.DIAG_HAUT, MovesList = new List<Move>() };
            GameLine diagBas = new GameLine() { Type = GameLineType.DIAG_BAS, MovesList = new List<Move>() };

            for (int x = 0; x < game.Size; x++) {
                for (int y = 0; y < game.Size; y++) {
                    line.MovesList.Add(GetMoveFromCoord(x, y));
                    column.MovesList.Add(GetMoveFromCoord(y, x));
                    GamePlate.Add(new Coord() { X = $"{x}", Y = $"{y}", Name = $"Field{x}I{y}" });
                    if(x == y)
                    {
                        diagHaut.MovesList.Add(GetMoveFromCoord(x, y));
                    }
                    if(x + y == game.Size - 1)
                    {
                        diagBas.MovesList.Add(GetMoveFromCoord(x, y));
                    }
                    if(y == game.Size - 1)
                    {
                        gameMatrix.Add(line);
                        line = new GameLine() { Type = GameLineType.VERTICALE, MovesList = new List<Move>() };
                        gameMatrix.Add(column);
                        column = new GameLine() { Type = GameLineType.HORIZONTALE, MovesList = new List<Move>() };
                    }
                }
            }
            gameMatrix.Add(diagHaut);
            gameMatrix.Add(diagBas);
        }

        async public Task<Player> FieldClick(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            return await Play(button);
        }

        async public Task<Player> Play(Button button)
        {
            if (!game.Started)
            {
                MessageBox.Show("En attente du second Joueur");
            }
            else if (game.Finished)
            {
                if (game.Winner == null)
                    MessageBox.Show("Match terminé sur une égalité");
                else
                {
                    winner = await ps.GetById(game.Winner.Value);
                    MessageBox.Show($"{winner.Name} a déjà remporté la victoire !!");

                }
            }
            else if (game.MovesList.Count(f => button.Name.Equals(f.Name)) == 0)
            {
                Move current = new Move();
                current.Name = button.Name;
                current.StyleType = game.CurrentPlayer.StyleType;
                button.Style = playersService.GetStyleFromStyleType(game.CurrentPlayer.StyleType);
                game.MovesList.Add(current);

                if (IsGameFinish())
                {
                    game.Finished = true;
                    if (game.Winner == null)
                        MessageBox.Show("Match nul");
                    else
                    {
                        winner = await ps.GetById(game.Winner.Value);
                        MessageBox.Show($"{winner.Name} remporte la victoire !!");
                    }
                    UpdateGamesCount();
                }
                else
                {
                    // switch player
                    game.CurrentPlayer = playerO.StyleType.Equals(game.CurrentPlayer.StyleType) ? playerX : playerO;
                    if (game.CurrentPlayer.Auto == true)
                    {
                        OrdiPlay();
                    }
                }
            }
            else if (game.MovesList.Count(f => button.Name.Equals(f.Name)) == 1)
            {
                MessageBox.Show("Cette case est déjà prise");
            }
            else
            {
                throw new IndexOutOfRangeException("More than one field detected");
            }

            UpdateParty(true);
            return game.CurrentPlayer;
        }

        public bool IsGameFinish()
        {
            FindWinner();
            return game.MovesList.Count() == game.Size * game.Size || game.Winner != null;
        }

        public int? FindWinner()
        {
            foreach(GameLine row in gameMatrix)
            {
                game.Winner = IsAWinnerFor(row.MovesList);
                if (game.Winner != null)
                    return game.Winner;
            }
            return null;
        }

        public int? IsAWinnerFor(List<Move> row)
        {
            IEnumerable<Move> diag = game.MovesList.Where(f => row.Any(m => m.Name == f.Name));
            if (diag.Count() == game.Size)
            {
                IEnumerable<string> playedStyleTypes = diag.Select(f => f.StyleType).Distinct();
                if (playedStyleTypes.Count() == 1)
                {
                    return GetPlayerByStyle(playedStyleTypes.First());
                }
            }
            return null;
        }

        public int? GetPlayerByStyle(string style)
        {
            return playerO.StyleType == style ? playerO.PlayerId : playerX.PlayerId;
        }

        async private void UpdateGamesCount()
        {
            if (game.Winner == null) {
                allPlayers.Single(p => playerO.Name.Equals(p.Name)).DrawGames++;
                allPlayers.Single(p => playerX.Name.Equals(p.Name)).DrawGames++;
            }
            else
            {
                winner = await ps.GetById(game.Winner.Value);
                if (playerO.Name.Equals(winner.Name))
                {
                    allPlayers.Single(p => playerO.Name.Equals(p.Name)).WinGames++;
                    allPlayers.Single(p => playerX.Name.Equals(p.Name)).LooseGames++;
                } 
                else
                {
                    allPlayers.Single(p => playerX.Name.Equals(p.Name)).WinGames++;
                    allPlayers.Single(p => playerO.Name.Equals(p.Name)).LooseGames++;
                }
            }
            allPlayers = await playersService.UpdateList(allPlayers);
        }

        async public void OrdiPlay()
        {
            await Play(ChooseMove(game.CurrentPlayer.Name == "OrdiFacile"));
        }

        public Button ChooseMove(bool easy = false)
        {
            if (easy)
            {
                return RandomChooseMove();
            }
            else
            {
                return IAChooseMove();
            }
        }

        private Button RandomChooseMove()
        {
            Random rnd = new Random();
            int loop = 0;
            List<int> alreadyTryed = new List<int>();
            Coord tryCoord;
            while(loop < game.Size)
            {
                string x = rnd.Next(game.Size).ToString();
                string y = rnd.Next(game.Size).ToString();
                tryCoord = GamePlate.SingleOrDefault(c => c.X == x && c.Y == y);
                if (tryCoord != null
                    && IsMoveAvailable(tryCoord.X, tryCoord.Y))
                {
                    Button btn = gameGrid.Children.OfType<Button>().Single(b => b.Name == tryCoord.Name);
                    return (btn);
                    //Play(btnList.Single(b => b.Tag.ToString().Equals(tryCoord.Name)));
                }
            }
            throw new Exception("Failed to choose");
        }

        private Button IAChooseMove()
        {
            Button btn = null;
            List<Button> btnList = new List<Button>();
            // test if can win
            btn = GetBtnIfWin();

            if (btn == null)
            {
                // test if opponent have a ligne with length > game.Size * 0.6666667
                btn = GetBtnIfOpponentOnlyOneMoveToVictory();
            }
            if (btn == null)
            {
                GameLine opponentLine = GetOpponentLineOverLevel();
                if (opponentLine != null)
                {
                    //if true : play at a end of the line
                    // try start first
                    btnList = GetPlayableButtonAtAEndOfLine(opponentLine);
                }
            }
            
            if (btn == null && !btnList.Any()) // if can't block opponent (or don't need) choose another move
            {
                // find all moves lines 
                List<GameLine> continuousLines = GetContinousLines(GetAllLinesFor(game.CurrentPlayer.StyleType));
                // ordered by length
                continuousLines.OrderByDescending(l => l.MovesList.Count());
                // while linesList.len > 0
                foreach(GameLine testedLine in continuousLines) {
                    // get button that can be played at ends of the current line
                    btnList = GetPlayableButtonAtAEndOfLine(testedLine);
                }
                // if no playable move
                if (!btnList.Any()) {
                    // return Random
                    btn = RandomChooseMove();
                }
                else
                { 
                    //save moveList
                    //List<Move> movesList_back = movesList.C
                    Button betterBtn = null;
                    double maxScore = 0;
                    double score;
                    Move testMove;
                    foreach (Button playableBtn in btnList)
                    {
                        // add move to test
                        testMove = new Move() { 
                            Name = playableBtn.Name,
                            StyleType = game.CurrentPlayer.StyleType
                        };
                        game.MovesList.Add(testMove);
                        // calculate score
                        score = CalculateGameScore(game.CurrentPlayer.StyleType);
                        if (score > maxScore)
                        {
                            betterBtn = playableBtn;
                            maxScore = score;
                        }
                        // remove 
                        game.MovesList.Remove(testMove);
                    }
                    btnList.Add(betterBtn);
                }
            }

            if (btnList.Any())
            {
                btn = btnList.First();
            }

            return btn;
        }

        private double CalculateGameScore(string playType)
        {
            double count = 0;
            List<GameLine> allLines = GetAllLinesFor(playType);
            // count square for mixedline (countinous or not) length
            foreach(GameLine line in allLines)
            {
                count += Math.Pow(line.MovesList.Count(), 2);
            }
            // continuous are more important so add point for it
            List<GameLine> allContinuousLines = GetContinousLines(GetAllLinesFor(playType));
            foreach(GameLine line in allContinuousLines)
            {
                count += Math.Pow(line.MovesList.Count(), 3);
            }
            return count;
        }

        private Coord GetEndCoords(GameLine line, string end)
        {
            Coord coord = end.Equals("lowerEnd") ? line.MovesList.First().Coordinates : line.MovesList.Last().Coordinates;
            int offset = end.Equals("lowerEnd") ? -1 : 1;
            switch(line.Type)
            {
                case GameLineType.HORIZONTALE:
                    coord.X = (Convert.ToInt32(coord.X) + offset).ToString();
                    break;
                case GameLineType.VERTICALE:
                    coord.Y = (Convert.ToInt32(coord.Y) + offset).ToString();
                    break;
                case GameLineType.DIAG_HAUT:
                    coord.X = (Convert.ToInt32(coord.X) + offset).ToString();
                    coord.Y = (Convert.ToInt32(coord.Y) + offset).ToString();
                    break;
                case GameLineType.DIAG_BAS:
                    coord.X = (Convert.ToInt32(coord.X) + offset).ToString();
                    coord.Y = (Convert.ToInt32(coord.Y) - offset).ToString();
                    break;
            }
            return coord;
        }

        private bool IsMoveAvailable(string x, string y)
        {
            // if coord out of range : return not available
            if (Convert.ToInt32(x) < 0 || Convert.ToInt32(y) < 0
                || Convert.ToInt32(x) >= game.Size || Convert.ToInt32(y) >= game.Size)
                return false;
            else
                return game.MovesList.Count(m => m.Name == $"Field{x}I{y}") == 0;
        }

        private int GetMaxLevelForOpponentLines()
        {
            return Convert.ToInt32(game.Size * 0.6666667);
        }

        private List<GameLine> GetAllLinesFor(string playType)
        {
            List<GameLine> lines = new List<GameLine>();
            foreach(GameLine matrixLine in gameMatrix)
            {
                GameLine line = new GameLine() { Type = matrixLine.Type };
                line.MovesList = game.MovesList.Where(m => m.StyleType == playType && matrixLine.MovesList.Any(glm => glm.Name == m.Name)).ToList();
                if (line.MovesList != null)
                    lines.Add(line);
            }
            return lines;
        }

        public List<GameLine> GetContinousLines(List<GameLine> playedLines)
        {
            List<GameLine> continuousLines = new List<GameLine>();
            GameLine continuousLine = new GameLine() { MovesList = new List<Move>() };

            foreach(GameLine playedLine in playedLines)
            {
                if (continuousLine.MovesList.Any())
                {
                    continuousLines.Add(continuousLine);
                }
                continuousLine = new GameLine() { Type = playedLine.Type, MovesList = new List<Move>() };

                foreach(Move move in playedLine.MovesList)
                {
                    if (continuousLine.MovesList.Count() == 0)
                    {
                        continuousLine.MovesList.Add(move);
                    } 
                    else
                    {
                        // parse coords
                        int lastX = Convert.ToInt32(continuousLine.MovesList.Last().Coordinates.X);
                        int lastY = Convert.ToInt32(continuousLine.MovesList.Last().Coordinates.Y);
                        int currentX = Convert.ToInt32(move.Coordinates.X);
                        int currentY = Convert.ToInt32(move.Coordinates.Y);
                        // test if this move next to last of line
                        if (Math.Abs(lastX - currentX) == 1 && Math.Abs(lastY - currentY) == 1)
                        {
                            // if next : continous so add to line
                            continuousLine.MovesList.Add(move);
                        }
                        else
                        {
                            // if not next : add current line and start new line with current move
                            continuousLines.Add(continuousLine);
                            continuousLine = new GameLine() { Type = playedLine.Type, MovesList = new List<Move>() { move } };
                        }
                    }
                }
            }
            return continuousLines;
        }

        private GameLine GetOpponentLineOverLevel()
        {
            string opponentType = game.CurrentPlayer.StyleType == ROUND_TYPE ? CROSS_TYPE : ROUND_TYPE;
            List<GameLine> oponentContinuousLines = GetContinousLines(GetAllLinesFor(opponentType));
            int overLevel = GetMaxLevelForOpponentLines();

            int max = 0;
            GameLine longestOverLine = null;
            foreach(GameLine line in oponentContinuousLines)
            {
                if (line.MovesList.Count >= overLevel && line.MovesList.Count() > max)
                {
                    max = line.MovesList.Count();
                    longestOverLine = line;
                }
            }
            return longestOverLine;
        }

        private List<Button> GetPlayableButtonAtAEndOfLine(GameLine line)
        {
            List<Button> playables = new List<Button>();
            Coord start = GetEndCoords(line, "lower");
            Coord end = GetEndCoords(line, "upper");
            if (IsMoveAvailable(start.X, start.Y))
            {
                playables.Add(gameGrid.Children.OfType<Button>().Single(b => b.Name == $"Field{start.X}I{start.Y}"));
            } 
            else if (IsMoveAvailable(end.X, end.Y))
            {
                playables.Add(gameGrid.Children.OfType<Button>().Single(b => b.Name == $"Field{end.X}I{end.Y}"));
            }
            return playables;
        }

        private Button GetBtnIfWin()
        {
            return GetBtnIfOnlyOneMoveToVictory(game.CurrentPlayer.StyleType);
        }

        private Button GetBtnIfOpponentOnlyOneMoveToVictory()
        {
            string opponentType = game.CurrentPlayer.StyleType == ROUND_TYPE ? CROSS_TYPE : ROUND_TYPE;
            return GetBtnIfOnlyOneMoveToVictory(opponentType);
        }

        private Button GetBtnIfOnlyOneMoveToVictory(string styleType) 
        {
            List<GameLine> playedLines = GetAllLinesFor(styleType);
            foreach(GameLine line in playedLines)
            {
                if (line.MovesList.Count() == game.Size - 1)
                {
                    Button btn = GetPlayableButtonInLine(line);
                    if(btn != null)
                    {
                        return btn;
                    }
                }
            }
            return null;
        }

        
        private Button GetPlayableButtonInLine(GameLine line) 
        {
            if (line.MovesList.Count() != game.Size - 1)
            {
                throw new Exception("Cette ligne ne contient pas le bon nombre de coups");
            }
            Move toMake = GetMatrixLineForGameLine(line).MovesList.Single(m => line.MovesList.Count(lm => lm.Name == m.Name) == 0);
            if (IsMoveAvailable(toMake.Coordinates.X, toMake.Coordinates.Y))
            {
                return gameGrid.Children.OfType<Button>().Single(b => b.Name == $"Field{toMake.Coordinates.X}I{toMake.Coordinates.Y}");
            }
            else
            {
                return null;
            }
        }

        private GameLine GetMatrixLineForGameLine(GameLine line)
        {
            return gameMatrix
                .Single(gl => gl.MovesList.Count(m => line.MovesList.Count(lm => lm.Name == m.Name) >= 1) == game.Size - 1);
        }
    }
}
