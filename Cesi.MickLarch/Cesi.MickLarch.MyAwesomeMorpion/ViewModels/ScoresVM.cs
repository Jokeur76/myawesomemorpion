﻿using Cesi.MickLarch.MyAwesomeMorpion.Services;
using MyAwesomeMorpionLibrary.Models;
using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cesi.MickLarch.MyAwesomeMorpion.ViewModels
{

    public class ScoresVM : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private bool graphVisibilty = false;

        public bool GraphVisibilty
        {
            get { return graphVisibilty; }
            set 
            { 
                graphVisibilty = value; 
                OnPropertyChanged();
            }
        }

        private ChartValues<double> draws = new ChartValues<double> { 0 };
        public ChartValues<double> Draws
        {
            get { return draws; }
            set
            {
                draws = value;
                OnPropertyChanged();
            }
        }

        private ChartValues<double> losses = new ChartValues<double> { 0 };
        public ChartValues<double> Losses
        {
            get { return losses; }
            set
            {
                losses = value;
                OnPropertyChanged();
            }
        }

        private ChartValues<double> victories = new ChartValues<double> { 0 };
        public ChartValues<double> Victories
        {
            get { return victories; }
            set
            {
                victories = value;
                OnPropertyChanged();
            }
        }

        private List<Player> allPlayers = new List<Player>();

        public List<Player> AllPlayers
        {
            get { return allPlayers; }
            set { allPlayers = value; }
        }


        private PlayersService playersService = new PlayersService();

        public ScoresVM()
        {
            loadPlayers();
            //GraphVisibilty = Visibility.Hidden.ToString();
        }

        async private void loadPlayers()
        {
            allPlayers = await playersService.GetPlayersList();
        }

        public void DisplayPieChart(Player player)
        {
            //pieChart.Series.Single(s => s.N)
            Losses = new ChartValues<double> { player.LooseGames };
            Victories = new ChartValues<double> { player.WinGames };
            Draws = new ChartValues<double> { player.DrawGames };
            GraphVisibilty = true;
        }
    }
}
