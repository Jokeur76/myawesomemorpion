﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using Cesi.MickLarch.MyAwesomeMorpion.Services;
using Cesi.MickLarch.MyAwesomeMorpion.Views;
using MyAwesomeMorpionLibrary.Models;
using Microsoft.AspNetCore.SignalR.Client;

namespace Cesi.MickLarch.MyAwesomeMorpion.ViewModels
{
    public class Main : INotifyPropertyChanged
    {
        const string ROUND_TYPE = "O";
        const string CROSS_TYPE = "X";

        private bool newGameIsEnabled = true;
        private bool resetGameIsEnabled = false;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public bool ResetGameIsEnabled
        {
            get => resetGameIsEnabled;
            set
            {
                resetGameIsEnabled = value;
                OnPropertyChanged();
            }
        }
        public bool NewGameIsEnabled
        {
            get => newGameIsEnabled;
            set
            {
                ResetGameIsEnabled = !value;
                newGameIsEnabled = value;
                OnPropertyChanged();
            }
        }

        private PlayersService playersService = new PlayersService();
        private List<Player> allPlayers;
        // socket
        HubConnection connection;

        public Main(List<Player> _allPlayers, HubConnection connection)
        {
            allPlayers = _allPlayers;
            // socket
            this.connection = connection;

        }

        async public void LaunchGame(ComboBox selectedX, TextBox joueurX, ComboBox selectedO, TextBox joueurO, Frame mainFrame, ComboBox gameSize)
        {
            int size = 3;
            Int32.TryParse(gameSize.Text, out size);
            Player playerO = null;
            Player playerX = null;
            if (joueurO.Text != "")
            {
                playerO = allPlayers.SingleOrDefault(p => p.Name == joueurO.Text);
                if (playerO == null)
                {
                    playerO = new Player(joueurO.Text, ROUND_TYPE, false);
                    allPlayers.Add(playerO);
                    allPlayers = await playersService.UpdateList(allPlayers);
                }
            }
            else
            {
                playerO = allPlayers.SingleOrDefault(p => p.Name == (string)selectedO.SelectedValue);
            }
            if (joueurX.Text != "")
            {
                playerX = allPlayers.SingleOrDefault(p => p.Name == joueurX.Text);
                if (playerX == null)
                {
                    playerX = new Player(joueurX.Text, CROSS_TYPE, false);
                    allPlayers.Add(playerX);
                    allPlayers = await playersService.UpdateList(allPlayers);
                }
            }
            else
            {
                playerX = allPlayers.SingleOrDefault(p => p.Name == (string)selectedX.SelectedValue);
            }

            if (playerO != null && playerX != null && playerO.Name != playerX.Name)
            {
                playerO.StyleType = ROUND_TYPE;
                playerX.StyleType = CROSS_TYPE;
                NewGameIsEnabled = false;
                var game = new Game();
                game.Size = size;
                mainFrame.Navigate(new VGame(new List<Player> { playerO, playerX }, game, connection));
            }
            else
            {
                MessageBox.Show("Vous devez renseigner deux joueurs différents");
            }
        }

        public void ResetGame(ComboBox selectedX, TextBox joueurX, ComboBox selectedO, TextBox joueurO, Frame mainFrame, ComboBox gameSize)
        {
            NewGameIsEnabled = true;
            LaunchGame(selectedX, joueurX, selectedO, joueurO, mainFrame, gameSize);
        }
        
    }
}
