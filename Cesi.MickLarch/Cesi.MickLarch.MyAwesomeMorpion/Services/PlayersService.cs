﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;
using System.Windows.Resources;
using MyAwesomeMorpionLibrary.Models;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Cesi.MickLarch.MyAwesomeMorpion.Services
{
    public class PlayersService: IPlayersService
    {
        private HttpClient client = new HttpClient();

        private Style ROUND_STYLE = (Style)Application.Current.Resources["FieldRond"];
        private Style CROSS_STYLE = (Style)Application.Current.Resources["FieldCross"];

        private List<Player> playersList = new List<Player>();

        public PlayersService()
        {
            client.BaseAddress = new Uri("https://localhost:44393/");
            client.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        
        public List<Player> PlayersList
        {
            get {

                return playersList.Select(p => new Player(p.Name, p.StyleType, p.Auto, p.DrawGames, p.WinGames, p.LooseGames)).ToList();
            }
            set {
                var test = value.Select(p => new Player(p.Name, p.StyleType, p.Auto, p.DrawGames, p.WinGames, p.LooseGames)).ToList();
                playersList = test;
            }
        }

        public async Task<List<Player>> GetPlayersList()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(
                    $"api/player");
                response.EnsureSuccessStatusCode();

                // Deserialize the updated product from the response body.
                string playersStr = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<Player>>(playersStr).ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Attention les joueurs n'ont pas pu être chargés");
                Console.WriteLine(ex);
                return new List<Player>();
            }
        }

        public async Task<List<Player>> GetConnectedPlayersList()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(
                    $"api/player/connected");
                response.EnsureSuccessStatusCode();

                // Deserialize the updated product from the response body.
                string playersStr = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<Player>>(playersStr).ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Attention la liste des joueurs connectés n'a pas pu être chargée");
                Console.WriteLine(ex);
                return new List<Player>();
            }
        }

        public async Task<List<Game>> GetProposalsListForPlayer(Player value)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(
                    $"api/player/" + value.PlayerId + "/games");
                response.EnsureSuccessStatusCode();

                // Deserialize the updated product from the response body.
                string gamesStr = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<Game>>(gamesStr).ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Attention la liste de vos propositions de jeux n'a pas pu être chargée");
                Console.WriteLine(ex);
                return new List<Game>();
            }
        }

        public async Task<Player> RegisterPlayer(Player value)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(
                    $"api/player", content);
                response.EnsureSuccessStatusCode();

                // Deserialize the updated product from the response body.
                string playerStr = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Player>(playerStr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("L'enregistrement a échoué veuillez recommencer");
                return null;
            }
        }

        public async Task<Player> ConnectPlayer(Player value)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync(
                    $"api/player/" + value.PlayerId + "/connect", content);
                response.EnsureSuccessStatusCode();

                // Deserialize the updated product from the response body.
                string playerStr = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Player>(playerStr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("La connexion a échoué veuillez recommencer");
                return null;
            }
        }

        public async Task<Player> GetById(int id)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(
                    $"api/player/" + id);
                response.EnsureSuccessStatusCode();

                // Deserialize the updated product from the response body.
                string playerStr = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Player>(playerStr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("La récupération du joueur a échoué veuillez recommencer");
                return null;
            }
        }

        public async Task<Player> GetByName(string name)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(
                    $"api/player/name/" + name);
                response.EnsureSuccessStatusCode();

                // Deserialize the updated product from the response body.
                string playerStr = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Player>(playerStr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("La récupération du joueur par son nom a échoué veuillez recommencer");
                return null;
            }
        }

        public async Task<bool> Disconnect(Player value)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync(
                    $"api/player/" + value.PlayerId + "/disconnect", content);
                response.EnsureSuccessStatusCode();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("La déconnexion a échoué veuillez recommencer");
                return false;
            }
        }

        async public Task<List<Player>> UpdateList(List<Player> allPlayers)
        {
            List<Player> updatedList = new List<Player>();
            try
            {
                foreach(Player value in allPlayers)
                {
                    var content = new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PutAsync(
                        $"api/player/" + value.PlayerId, content);
                    response.EnsureSuccessStatusCode();
                    // Deserialize the updated product from the response body.
                    string playerStr = await response.Content.ReadAsStringAsync();
                    updatedList.Add(JsonConvert.DeserializeObject<Player>(playerStr));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("La mise a jour des joueurs a échoué veuillez recommencer");
            }
            return updatedList;
        }

        async public Task<List<Game>> GetAcceptedFor(int playerId)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(
                    $"api/player/" + playerId + "/accepted");
                response.EnsureSuccessStatusCode();
                // Deserialize the updated product from the response body.
                string playerStr = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<Game>>(playerStr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("La mise a jour des joueurs a échoué veuillez recommencer");
                return new List<Game>();
            }
        }

        public Style GetStyleFromStyleType(string type)
        {
            if (type == "O")
                return ROUND_STYLE;
            else
                return CROSS_STYLE;
        }
    }
}
