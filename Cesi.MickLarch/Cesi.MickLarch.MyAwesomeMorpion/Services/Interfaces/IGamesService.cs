﻿using MyAwesomeMorpionLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cesi.MickLarch.MyAwesomeMorpion.Services
{
    interface IGamesService
    {
        Task<List<Game>> GetGamesList();
        
        Task<bool> AskForGame(Player connectedPlayer, Player selectedPlayer);

        Task<Game> Update(Game game);
        
        Task<List<Game>> UpdateList(List<Game> gamesList);

        Task<Game> GetById(int id);
    }
}
