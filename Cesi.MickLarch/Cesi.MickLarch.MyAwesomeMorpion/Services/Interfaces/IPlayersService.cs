﻿using MyAwesomeMorpionLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cesi.MickLarch.MyAwesomeMorpion.Services
{
    interface IPlayersService
    {
        Task<List<Player>> GetPlayersList();
        
        Task<Player> RegisterPlayer(Player value);

        Task<List<Player>> UpdateList(List<Player> allPlayers);

        Task<Player> ConnectPlayer(Player value);

        Task<List<Game>> GetProposalsListForPlayer(Player value);
        
        Task<List<Game>> GetAcceptedFor(int playerId);
    }
}
