﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;
using System.Windows.Resources;
using MyAwesomeMorpionLibrary.Models;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Cesi.MickLarch.MyAwesomeMorpion.Services
{

    public class GamesService: IGamesService
    {
        private HttpClient client = new HttpClient();

        public GamesService()
        {
            client.BaseAddress = new Uri("https://localhost:44393/");
            client.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<Game>> GetGamesList()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(
                    $"api/game");
                response.EnsureSuccessStatusCode();

                // Deserialize the updated product from the response body.
                string gamesStr = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<Game>>(gamesStr).ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Attention les jeux n'ont pas pu être chargés");
                Console.WriteLine(ex);
                return new List<Game>();
            }
        }

        async public Task<List<Game>> UpdateList(List<Game> allGames)
        {
            List<Game> updatedList = new List<Game>();
            try
            {
                foreach(Game value in allGames)
                {
                    var content = new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PutAsync(
                        $"api/game/" + value.GameId, content);
                    response.EnsureSuccessStatusCode();
                    // Deserialize the updated product from the response body.
                    string GameStr = await response.Content.ReadAsStringAsync();
                    updatedList.Add(JsonConvert.DeserializeObject<Game>(GameStr));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("La mise a jour des jeux a échoué veuillez recommencer");
            }
            return updatedList;
        }

        async public Task<Game> Update(Game value)
        {
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync(
                    $"api/game/" + value.GameId, content);
                response.EnsureSuccessStatusCode();
                // Deserialize the updated product from the response body.
                string GameStr = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Game>(GameStr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("La mise a jour du jeu a échoué veuillez recommencer");
                return null; 
            }
        }

        async public Task<bool> AskForGame(Player connectedPlayer, Player selectedPlayer)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(
                    $"api/game/new/" + connectedPlayer.PlayerId + "/" + selectedPlayer.PlayerId);
                response.EnsureSuccessStatusCode();
                
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("La demande de jeu a échoué veuillez recommencer");
                return false;
            }
        }

        async public Task<Game> GetById(int id)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(
                    $"api/game/" + id);
                response.EnsureSuccessStatusCode();

                // Deserialize the updated product from the response body.
                string GameStr = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Game>(GameStr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("La recherche du jeu a échoué veuillez recommencer");
                return null;
            }
        }
    }
}
